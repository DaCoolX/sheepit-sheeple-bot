package bot.interactionresponders;

import bot.commands.Command;
import bot.settings.Settings;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.exceptions.ErrorHandler;

import java.util.concurrent.TimeUnit;

public class MustBeSheepminResponder extends BaseInteractionResponder {

    public MustBeSheepminResponder(Settings settings) {
        super(settings);
    }

    @Override
    public void handle(SlashCommandInteractionEvent event, Command command) {
        StringBuilder errorMsg = new StringBuilder(String.format("You must have the %s permission to do that or have one of the following roles: ",
                command.getRequiredPermission()));
        for (String id : Settings.getInstance().getSuperRoleIDs()) {
            errorMsg.append(event.getGuild().getRoleById(id).getName()).append(" ");
        }
        event.replyEmbeds(settings.createMessage(errorMsg.toString())).queue(
                m -> m.deleteOriginal().queueAfter(settings.getDeleteDelaySeconds(), TimeUnit.SECONDS,
                        null, new ErrorHandler())
        );
    }
}
