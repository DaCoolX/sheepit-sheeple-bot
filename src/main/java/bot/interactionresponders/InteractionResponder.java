package bot.interactionresponders;

import bot.commands.Command;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public interface InteractionResponder {
    void handle(SlashCommandInteractionEvent event, Command command);
}
