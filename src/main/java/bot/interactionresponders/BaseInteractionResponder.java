package bot.interactionresponders;

import bot.settings.Settings;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public abstract class BaseInteractionResponder implements InteractionResponder {
    protected final Settings settings;
}
