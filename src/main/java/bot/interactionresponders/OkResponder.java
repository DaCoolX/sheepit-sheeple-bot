package bot.interactionresponders;

import bot.commands.Command;
import bot.errorhandlers.FailedInteractionErrorHandler;
import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.exceptions.ErrorHandler;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class OkResponder extends BaseInteractionResponder {

    public OkResponder(Settings settings) {
        super(settings);
    }

    @Override
    public void handle(SlashCommandInteractionEvent event, Command command) {
        synchronized (settings.getGlobalLock()) {
            Optional<MessageEmbed> response = command.handle(event);

            if(event.getChannel().getId().equals(settings.getRoleChannelID()) && response.isPresent() &&!event.getHook().isExpired()) {
                event.replyEmbeds(response.get()).setEphemeral(command.isEphemeral()).queue(
                        msg -> msg.deleteOriginal().queueAfter(settings.getDeleteDelaySeconds(), TimeUnit.SECONDS,
                                null, new ErrorHandler())
                );
            }
            else if (response.isPresent() && !event.getHook().isExpired()) {
                if(event.isAcknowledged()) {
                    event.getHook().sendMessageEmbeds(response.get()).queue(null, new FailedInteractionErrorHandler());
                }
                else {
                    event.replyEmbeds(response.get()).queue(null, new FailedInteractionErrorHandler());
                }
            }
        }
    }
}
