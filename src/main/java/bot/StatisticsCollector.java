package bot;

import bot.graphing.Graph;
import bot.graphing.GraphPlotter;
import bot.graphing.RolloverIntegerArray;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class StatisticsCollector {

    public static final String DIAGRAM_IMAGE_FILE_NAME = "statistics.png";
    public static final int PRESERVE_OLD_VALUES = 14;

    private Graph newlyJoinedMembers;
    private Graph newlyRegisteredMembers;
    private Graph autoKickedMembers;
    private GraphPlotter plotter;

    public StatisticsCollector() {
        this.newlyJoinedMembers =  new Graph("Newly joined", Color.CYAN, new RolloverIntegerArray(PRESERVE_OLD_VALUES));
        this.newlyRegisteredMembers = new Graph("Newly registered", Color.RED, new RolloverIntegerArray(PRESERVE_OLD_VALUES));
        this.autoKickedMembers = new Graph("Auto kicked", Color.ORANGE, new RolloverIntegerArray(PRESERVE_OLD_VALUES));
        this.plotter = new GraphPlotter();


        //DEBUG
//        List<Integer> debug_joined = new ArrayList<>();
//        for (int i = 0; i < PRESERVE_OLD_VALUES; i++) {
//            debug_joined.add((int) (Math.random() * 40));
//        }
//        this.newlyJoinedMembers.setValues(debug_joined);
    }

    public int getCapacity() {
        return PRESERVE_OLD_VALUES;
    }
    public synchronized int getNewlyJoinedMembers() {
        return newlyJoinedMembers.getNewestValue();
    }

    public synchronized void setNewlyJoinedMembers(int newlyJoinedMembers) {
        this.newlyJoinedMembers.setNewestValue(newlyJoinedMembers);
    }

    public synchronized int getNewlyRegisteredMembers() {
        return newlyRegisteredMembers.getNewestValue();
    }

    public synchronized void setNewlyRegisteredMembers(int newlyRegisteredMembers) {
        this.newlyRegisteredMembers.setNewestValue(newlyRegisteredMembers);
    }

    public synchronized int getAutoKickedMembers() {
        return autoKickedMembers.getNewestValue();
    }

    public synchronized void setAutoKickedMembers(int autoKickedMembers) {
        this.autoKickedMembers.setNewestValue(autoKickedMembers);
    }

    /**
     * Resets the current counters. The old values will be retained for a number of times this method is called determined by {@link #PRESERVE_OLD_VALUES}
     * for graph plotting
     */
    public synchronized void rollover() {
        this.newlyJoinedMembers.rollover();
        this.newlyRegisteredMembers.rollover();
        this.autoKickedMembers.rollover();
    }

    public synchronized BufferedImage plotGraphs(List<String> abscissaLabels) throws RuntimeException {
        try {
            var image = plotter.plotGraphs(
                    abscissaLabels,
                    "Joined Members Statistics",
                    newlyRegisteredMembers,
                    newlyJoinedMembers,
                    autoKickedMembers
            );
            return image;
        }
        catch (IOException | FontFormatException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized BufferedImage plotGraphs() throws RuntimeException {
        var labels = createDateLabels(PRESERVE_OLD_VALUES);
        var image = plotGraphs(labels);
        return image;
    }

    public File saveGraph(String filename, List<String> abscissaLabels) {
        var image = plotGraphs(abscissaLabels);
        try {
            return plotter.saveImage(filename, image);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public File saveGraph() {
        var image = plotGraphs();
        try {
            return plotter.saveImage(DIAGRAM_IMAGE_FILE_NAME, image);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private List<String> createDateLabels(int numberOfLabels) {
        LocalDate date = LocalDate.now().minusDays(numberOfLabels - 1);
        List<String> labels = new ArrayList<>(numberOfLabels);
        var format = DateTimeFormatter.ofPattern("d MMM", Locale.ENGLISH);

        for(int i = 0; i < numberOfLabels; i++) {
            labels.add(i, date.format(format));
            date = date.plusDays(1);
        }
        return labels;
    }
}
