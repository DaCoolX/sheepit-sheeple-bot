package bot.logging;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LogFormatter extends Formatter {
    private final String LOG_DATE_FORMAT = "dd-MM-yyyy hh:mm:ss";
    private SimpleDateFormat dateFormat;

    public LogFormatter() {
        dateFormat = new SimpleDateFormat(LOG_DATE_FORMAT);
    }

    @Override
    public String format(LogRecord logRecord) {
        String date = dateFormat.format(new Date(logRecord.getMillis()));

        return String.format("[%s] %s %s %s: %s",
                date,
                logRecord.getLevel(),
                logRecord.getSourceClassName(),
                logRecord.getSourceMethodName(),
                logRecord.getMessage());
    }
}
