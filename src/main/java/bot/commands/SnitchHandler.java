package bot.commands;

import bot.settings.Settings;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;

import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class SnitchHandler extends BaseCommandHandler {

    private static final String OPTION_NAME = "user";

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandInteractionEvent event) {
        var option = event.getOption(OPTION_NAME);
        if (option == null) {
            return Optional.empty();
        }

        return Optional.of(getInfo(option.getAsMember(), event.getMember()));
    }

    private MessageEmbed getInfo(Member member, Member caller) {
        MessageEmbed.Field reasonField = null;

        MessageEmbed.Field userField = new MessageEmbed.Field("Member: ", String.format("%s | %s | %s", member.getAsMention(), member.getEffectiveName(), member.getId()), true);
        MessageEmbed.Field serverJoinField = new MessageEmbed.Field("Server Join: ", member.getTimeJoined().format(DateTimeFormatter.RFC_1123_DATE_TIME), false);
        MessageEmbed.Field discordJoinField = new MessageEmbed.Field("Discord Join: ", member.getUser().getTimeCreated().format(DateTimeFormatter.RFC_1123_DATE_TIME), true);
        String avatar = member.getUser().getEffectiveAvatarUrl();
        String footer = String.format("Requested by %s", caller.getUser().getAsTag());

        MessageEmbed.Field[] fields = new MessageEmbed.Field[4];
        fields[0] = userField;
        fields[1] = serverJoinField;
        fields[2] = discordJoinField;
        if(reasonField != null) fields[3] = reasonField;

        MessageEmbed report = Settings.getInstance().createMessage("", "Member Info", avatar, footer, fields);
        return report;
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        guild.upsertCommand(command.getName(), command.getDescription())
                .addOption(OptionType.USER, OPTION_NAME, "Which user do you want to know more about", true)
                .queue();
    }
}