package bot.commands;

import bot.commands.roleCommands.*;
import bot.settings.Settings;
import lombok.Getter;
import net.dv8tion.jda.api.Permission;

@Getter
public enum DefaultCommands {

    RESET("sheeple-reset", "It's been **%d** days since someone asked about running SheepIt on a RaspberryPi.\nCounter reset to 0\n\nSheepit only supports official builds " +
            "of Blender, of which there are currently none for ARM Processors. Apart from that, the Pi is " +
            "too weak to render most frames in under 2 hours. With a power rating of 4%%, a normal frame " +
            "(30 minutes on the reference machine) would take 12,5h.",
            "Reset days counter",
            CommandCategories.PI,
            Permission.MANAGE_CHANNEL,
            new ResetHandler(),
            true,
            false),

    COUNT("sheeple-count",
            "It's been **%d** days since someone asked about running SheepIt on a RaspberryPi.",
            "Count days since last incident",
            CommandCategories.PI,
            Permission.MESSAGE_SEND, new CountHandler(),
            true,
            false),

    HELP("sheeple-help",
            "Here is a list of all available commands: \n%s",
            "List all available commands",
            CommandCategories.MISC,
            Permission.MESSAGE_SEND,
            new HelpHandler(),
            false,
            false),

    TWIT("sheeple-twit",
            "%s tried to reset the counter. They will be ignored for 30 minutes. The counter remains unchanged",
            "Troll deterrent",
            CommandCategories.MISC,
            Permission.MANAGE_CHANNEL,
            new TwitHandler(),
            true,
            false),

    STATS("sheeple-stats",
            "Webserver Load: %s\nWebserver Overload: %s\nFrames remaining: %s\nFrames rendering: %s\nConnected clients: %s\nActive Projects: %s",
            "Show statistics about the farm",
            CommandCategories.MISC,
            Permission.MESSAGE_SEND,
            new SheepItStatsHandler(),
            true,
            false),

    SHUTDOWN("shutdown",
            "",
            "Stops the bot",
            CommandCategories.BOT_MANAGEMENT,
            Permission.MANAGE_SERVER,
            new ShutdownHandler(),
            true,
            true),

    ARREST("arrest",
            "",
            "Put a victim into confinement and delete all of their recent messages",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MANAGE_ROLES,
            new ArrestHandler(),
            true,
            false),

    WIPE("wipe",
            "",
            "If you can execute it you know what it does",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MANAGE_ROLES,
            new WipeHandler(),
            true,
            true),

    SHEEPLE("sheeple-sheeple",
            "",
            "Where Sheeple came from (not really)",
            CommandCategories.MISC,
            Permission.MESSAGE_SEND,
            new SheepleHandler(),
            true,
            false),

    LOBBY_BUTTON("lobby-button",
            "",
            "Adds a button for registration",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MANAGE_SERVER,
            new LobbyButtonHandler(),
            true,
            false
            ),

    SNITCH("snitch",
            "",
            "Show some general info about a server member",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MANAGE_ROLES,
            new SnitchHandler(),
            true,
            false),

    GUILD_STATS("guild-stats",
            "",
            "Show some gathered statistics about the server"
            , CommandCategories.SERVER_MANAGEMENT,
            Permission.MANAGE_ROLES,
            new ServerStatsHandler(),
            true,
            false),

    COPY_HISTORY("copy-history",
            "",
            "Copies message history from one channel to another one that the bot can see",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.ADMINISTRATOR,
            new CopyHistoryHandler(),
            true,
            false);


    private Command cmd;

    DefaultCommands(String cmdName,
                    String cmdOutput,
                    String cmdDescription,
                    CommandCategories category,
                    Permission requiredPermission,
                    CommandHandler handler,
                    boolean isDefaultCommand,
                    boolean isEphemeral) {
        this.cmd = new Command(cmdName, cmdOutput, cmdDescription, category, requiredPermission,
                handler, isDefaultCommand, isEphemeral);
    }
}
