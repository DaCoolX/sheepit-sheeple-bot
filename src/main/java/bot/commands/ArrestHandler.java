package bot.commands;

import bot.errorhandlers.MessageDeleteErrorHandler;
import bot.settings.Settings;
import lombok.NonNull;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.entities.channel.concrete.ThreadChannel;
import net.dv8tion.jda.api.entities.channel.middleman.GuildMessageChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.exceptions.HierarchyException;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ArrestHandler extends BaseCommandHandler {

    public static final String COMMAND_NAME = "arrest";
    private static final Duration DELETE_YOUNGER_THAN = Duration.ofHours(6); //only check messages from within the last X ms
    private static final OptionData OPTION_VICTIM =
            new OptionData(OptionType.USER, "victim", "The member you want to arrest", true);
    private static final OptionData OPTION_REASON =
            new OptionData(OptionType.STRING, "reason", "Short explanation for the arrest", false);

    private ExecutorService executor;

    private JDA api;

    public ArrestHandler() {
        super();
        executor = Executors.newSingleThreadExecutor();
    }

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandInteractionEvent event) {
        event.deferReply().queue(null, null);
        Guild guild = event.getGuild();
        Logger log = Logger.getLogger(Settings.LOGGER_NAME);
        Member victim = getVictim(event);
        if (victim == null) {
            throw new IllegalArgumentException("Victim is null!");
        }

        try {
            Role mutedRole = guild.getRoleById(Settings.getInstance().getMutedRoleID());
            if (mutedRole == null) {
                throw new IllegalArgumentException("Muted role returned null. Couldnt retrieve. mutedRoleID: " + Settings.getInstance().getMutedRoleID());
            }
            try {
                if (guild.getSelfMember().canInteract(victim)) {
                    guild.modifyMemberRoles(victim, mutedRole).queue();
                }
                else {
                    return Optional.of(Settings.getInstance().createMessage("Cannot manage roles of this member. This is most likely due to a hierarchy or permission issue"));
                }
            }
            catch(InsufficientPermissionException | HierarchyException | IllegalArgumentException e) {
                log.log(Level.SEVERE, e, () -> "Exception while trying to arrest member: " + victim);
                return Optional.of(Settings.getInstance().createMessage("Exception while trying to arrest member: " + victim + ". " + e.getMessage()));
            }

            var textChannels = guild.getTextChannelCache().stream()
                    .filter(Objects::nonNull)
                    .filter(c -> guild.getSelfMember().hasPermission(c, Permission.MESSAGE_MANAGE))
                    .collect(Collectors.toList());
            var threads = guild.getThreadChannelCache().stream()
                    .filter(Objects::nonNull)
                    .filter(t -> guild.getSelfMember().hasPermission(t, Permission.MESSAGE_MANAGE))
                    .collect(Collectors.toList());

            OffsetDateTime date = OffsetDateTime.now().minus(DELETE_YOUNGER_THAN);

            executor.submit(() -> {
                for (TextChannel channel : textChannels) {
                    deleteMessages(victim, channel, date);
                }

                for (ThreadChannel thread : threads) {
                    deleteMessages(victim, thread, date);
                }
            });

            MessageEmbed response = Settings.getInstance().createMessage(
                    String.format(
                            "%s has been arrested. Deleting all messages from within the last %d hours.",
                            victim.getUser().getAsTag(),
                            DELETE_YOUNGER_THAN.toHours()
                    )
            );

            String logChannelID = Settings.getInstance().getLogChannelID();
            TextChannel channel = guild.getTextChannelById(logChannelID);
            if (Objects.isNull(channel)) {
                log.log(Level.WARNING, () -> String.format("Report channel not found: %s", logChannelID));
            }
            else {
                printVictimInfo(channel, victim, event.getMember(), getReason(event));
            }
            return Optional.of(response);
        } catch (IllegalArgumentException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e, () -> "An exception occurred: ");
            return Optional.of(Settings.getInstance().createMessage("An exception occurred: " + e.getMessage()));
        }
    }

    private Member getVictim(SlashCommandInteractionEvent event) {
        var victimOption = event.getOption(OPTION_VICTIM.getName());
        if (Objects.isNull(victimOption)) {
            return null;
        }
        return victimOption.getAsMember();
    }

    private String getReason(SlashCommandInteractionEvent event) {
        var reason = event.getOption(OPTION_REASON.getName());
        if (Objects.isNull(reason)) {
            return null;
        }
        return reason.getAsString();
    }

    public boolean deleteMessages(Member victim, GuildMessageChannel channel, OffsetDateTime messagesYoungerThan) {
        long memberID = victim.getIdLong();
        Logger log = Logger.getLogger(channel.getGuild().getId());

        try {
            MessageHistory history = channel.getHistory();
            List<Message> messages = null;
            boolean stop = false;

            do {
                messages = history.retrievePast(100).complete();
                stop = messages.isEmpty() || messages.get(messages.size() - 1).getTimeCreated().isBefore(messagesYoungerThan);

                var toBeDeleted = messages.stream()
                        .filter(message -> message.getAuthor().getIdLong() == memberID && message.getTimeCreated().isAfter(messagesYoungerThan))
                        .collect(Collectors.toList());

                channel.purgeMessages(toBeDeleted);
            } while (!stop);
        }
        catch (InsufficientPermissionException | IllegalArgumentException e) {
            log.log(Level.SEVERE, () -> String.format("Exception: %s%nVictim: %s; channel: %s; messagesYoungerThan: %s",
                    e.getMessage(),
                    victim.getUser().getAsMention(),
                    channel.getAsMention(),
                    messagesYoungerThan
            ));
            return false;
        }
        return true;
    }

    private void printVictimInfo(@NonNull TextChannel channel, @NonNull Member victim, @NonNull Member caller, String reason) {

        MessageEmbed.Field reasonField = null;
        if(Objects.nonNull(reason)) {
            reasonField = new MessageEmbed.Field("Arrest reason: ", reason, true);
        }

        MessageEmbed.Field userField = new MessageEmbed.Field("Victim: ", String.format("%s | %s | %s", victim.getAsMention(), victim.getEffectiveName(), victim.getId()), true);
        MessageEmbed.Field serverJoinField = new MessageEmbed.Field("Server Join: ", victim.getTimeJoined().format(DateTimeFormatter.RFC_1123_DATE_TIME), false);
        MessageEmbed.Field discordJoinField = new MessageEmbed.Field("Discord Join: ", victim.getUser().getTimeCreated().format(DateTimeFormatter.RFC_1123_DATE_TIME), true);
        String avatar = victim.getUser().getEffectiveAvatarUrl();
        String footer = String.format("Arrested by %s", caller.getUser().getAsTag());

        MessageEmbed.Field[] fields = new MessageEmbed.Field[4];
        fields[0] = userField;
        fields[1] = serverJoinField;
        fields[2] = discordJoinField;
        if(reasonField != null) fields[3] = reasonField;

        MessageEmbed report = Settings.getInstance().createMessage("", "Inmate record", avatar, footer, fields);
        channel.sendMessageEmbeds(report).queue();
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        //snag away the api reference
        this.api = api;

        var slashCommand = Commands.slash(command.getName(), command.getDescription())
                        .addOptions(OPTION_REASON, OPTION_VICTIM);

        api.upsertCommand(slashCommand).queue();
    }

    public void registerCommand(Command command, Guild guild) {
        //snag away the api reference
        this.api = guild.getJDA();

        var slashCommand = Commands.slash(command.getName(), command.getDescription())
                .addOptions(OPTION_VICTIM, OPTION_REASON);

        guild.upsertCommand(slashCommand).queue();
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandInteractionEvent event) {
        try {
            Member victim = event.getOption(OPTION_VICTIM.getName()).getAsMember();
            if (victim == null) return RequirementsCheckResult.ERROR;

            if (victim.hasPermission(Permission.ADMINISTRATOR) || victim.hasPermission(command.getRequiredPermission())) {
                event.replyEmbeds(
                                Settings.getInstance()
                                        .createMessage("You cannot arrest a member with the " + command.getRequiredPermission() + " permission"))
                        .queue(m -> m.deleteOriginal().queueAfter(Settings.getInstance().getDeleteDelaySeconds(), TimeUnit.SECONDS,
                                null, new MessageDeleteErrorHandler()));
                return RequirementsCheckResult.IGNORE;
            }

            List<Role> sheepItRoles = Settings.getInstance().getSuperRoleIDs().stream()
                    .map(r -> event.getGuild().getRoleById(r)).collect(Collectors.toList());

            boolean callerHasSheepItRole = !Collections.disjoint(event.getMember().getRoles(), sheepItRoles);
            if (callerHasSheepItRole || super.meetsRequirements(command, event) == RequirementsCheckResult.OK)
                return RequirementsCheckResult.OK;
            else
                return RequirementsCheckResult.MUST_BE_SHEEPMIN;

        } catch (NullPointerException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e, () -> "Exception while checking requirements: ");
            return RequirementsCheckResult.ERROR;
        }
    }
}
