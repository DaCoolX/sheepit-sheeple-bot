package bot.commands;

import bot.settings.Settings;
import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.WebhookClientBuilder;
import club.minnced.discord.webhook.send.AllowedMentions;
import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.Webhook;
import net.dv8tion.jda.api.entities.channel.concrete.NewsChannel;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.entities.channel.middleman.StandardGuildMessageChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.requests.ErrorResponse;

import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;

public class CopyHistoryHandler extends BaseCommandHandler {

    private static final int HOOK_COUNT = 1;

    private static final OptionData OPTION_CHANNEL_FROM = new OptionData(OptionType.STRING, "copy-from-channel-id",
            "ID of the channel to copy the message history from", true);
    private static final OptionData OPTION_CHANNEL_TO = new OptionData(OptionType.STRING, "copy-to-channel-id",
            "ID of the channel to copy the message history to", true);

    private static final OptionData OPTION_START_FROM = new OptionData(OptionType.STRING, "start-after-msg-id",
            "ID of the oldest message in the chat history to start after", false);

    public CopyHistoryHandler() {
    }

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandInteractionEvent event) {
//        event.deferReply().queue();
        Logger log = Logger.getLogger(Settings.LOGGER_NAME);

        String channelFromID = event.getOption(OPTION_CHANNEL_FROM.getName(), "", OptionMapping::getAsString);
        String channelToID = event.getOption(OPTION_CHANNEL_TO.getName(), "", OptionMapping::getAsString);
        String startFrom = event.getOption(OPTION_START_FROM.getName(), null, OptionMapping::getAsString);

        if (channelFromID.isBlank()) {
            return Optional.of(Settings.getInstance()
                    .createMessage("You need to provide a channel to copy from"));
        }
        if (channelToID.isBlank()) {
            return Optional.of(Settings.getInstance()
                    .createMessage("You need to provide a channel to copy to"));
        }

        event.deferReply().queue();

        StandardGuildMessageChannel channelFrom = retrieveGuildMessageChannel(event.getJDA(), channelFromID);
        StandardGuildMessageChannel channelTo = retrieveGuildMessageChannel(event.getJDA(), channelToID);
        event.getHook().editOriginalEmbeds(Settings.getInstance().createMessage("Creating webooks...")).queue();

        if (channelFrom == null) {
            return Optional.of(Settings.getInstance().createMessage("No text or news channel with ID " + channelFromID + " was found"));
        }
        if (channelTo == null) {
            return Optional.of(Settings.getInstance().createMessage("No text or news channel with ID " + channelToID + " was found"));
        }

        List<WebhookClient> webhookClients = createWebhookClients(channelTo, HOOK_COUNT);
        if (webhookClients.isEmpty()) {
            event.getHook().editOriginalEmbeds(Settings.getInstance().createMessage("Failed to create webhook. Please check the logs for more info")).queue();
            return Optional.empty();
        }
        else {
            event.getHook().editOriginalEmbeds(Settings.getInstance().createMessage("Could only create " +
                    webhookClients.size() + "/" + HOOK_COUNT + " webhooks. Transfer will take longer...")).queue();
        }


        int counter = 0;
        MessageHistory history = null;
        try {
            if (startFrom == null) {
                history = channelFrom.getHistoryFromBeginning(1).complete();
            }
            else {
                try {
//                    channelFrom.retrieveMessageById(startFrom).complete();
                    history = channelFrom.getHistoryAfter(startFrom, 1).complete();
                }
                catch (Exception e) {
                    event.getHook().editOriginalEmbeds(Settings.getInstance().createMessage(e.getCause() + ": " + e.getMessage())).queue();
                    return Optional.empty();
                }
            }
        }
        catch (ErrorResponseException e) {
            event.getHook().editOriginalEmbeds(Settings.getInstance().createMessage("Error retrieving channel history: " + e.getMessage())).queue();
            return Optional.empty();
        }
        catch (Exception e) {
            return Optional.of(Settings.getInstance().createMessage("Unknown exception occured: " + e.getCause() + ": " + e.getMessage()));
        }

        event.getHook().editOriginalEmbeds(Settings.getInstance().createMessage("Copying...")).queue();
        WebhookMessageBuilder messageBuilder = new WebhookMessageBuilder();

        long eventChannelID = event.getChannel().getIdLong();
        List< CompletableFuture< club.minnced.discord.webhook.receive.ReadonlyMessage> > futures = null;
        JDA jda = event.getJDA();
        HttpClient httpClient = HttpClient.newHttpClient();

        do {
            var retrievedHistory = history.getRetrievedHistory();
            if (retrievedHistory.isEmpty()) {
                var eventChannel = jda.getTextChannelById(eventChannelID);
                if (eventChannel != null) {
                    eventChannel.sendMessageEmbeds(Settings.getInstance().createMessage("Copied " + counter + " message from " + channelFrom.getGuild().getName() + "#" + channelFrom.getName()
                            + " to " + channelTo.getGuild().getName() + "#" + channelTo.getName())).queue();
                }
                return Optional.empty();
            }

            for (int i = retrievedHistory.size() - 1; i >= 0; i--) {
                var msg = retrievedHistory.get(i);
                if (msg.getContentRaw().isBlank()) {
                    if (!msg.getEmbeds().isEmpty()) {
                        //copy embeds
                        for (var embed : msg.getEmbeds()) {
                            messageBuilder.addEmbeds(createWebhookEmbed(embed));
                        }
                    }
                    else {
//                        var files = msg.getAttachments();
//                        if (!files.isEmpty()) {
//                            for (var file : files) {
//                                var request = HttpRequest.newBuilder()
//                                        .uri(URI.create(file.getUrl()))
//                                        .GET()
//                                        .build();
//
//                                try {
//                                    var response = httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());
//                                    if (response.statusCode() == HttpURLConnection.HTTP_OK) {
//                                        messageBuilder.addFile(file.getFileName(), response.body());
//                                    }
//                                    else {
//                                        throw new IOException("Unexpected status code " + response.statusCode());
//                                    }
//                                }
//                                catch (IOException e) {
//                                    e.printStackTrace();
//                                    log.log(Level.WARNING, () -> e.getCause() + e.getMessage() + " - " + file.getUrl());
//                                }
//
//                                catch (InterruptedException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }
                        continue;
                    }
                }

                try {
                    var webhookMessage = messageBuilder
                            .setAvatarUrl(msg.getAuthor().getAvatarUrl())
                            .setUsername(msg.getAuthor().getName())
                            .setContent(msg.getContentRaw())
                            .setAllowedMentions(AllowedMentions.none())
                            .build();

                    webhookClients.get(counter % webhookClients.size()).send(webhookMessage);
                    counter++;
                    messageBuilder.reset();
                    messageBuilder.resetFiles();
                    messageBuilder.resetEmbeds();
                }
                catch (ErrorResponseException e) {
                    System.err.println(e.getCause() + ": " + e.getMessage());
                }
            }
            history = channelFrom.getHistoryAfter(history.getRetrievedHistory().get(0), 100).complete();
        } while (!history.isEmpty());

        return Optional.empty();
    }

    private WebhookEmbed createWebhookEmbed(MessageEmbed embed) {
        var embedFields = new ArrayList<WebhookEmbed.EmbedField>();
        for (var field : embed.getFields()) {
            embedFields.add(new WebhookEmbed.EmbedField(
                    field.isInline(),
                    Optional.ofNullable(field.getName()).orElse("Field"),
                    Optional.ofNullable(field.getValue()).orElse("Field"))
            );
        }

        return new WebhookEmbed(
                embed.getTimestamp(),
                embed.getColorRaw(),
                embed.getDescription(),
                embed.getThumbnail() != null ? embed.getThumbnail().getUrl() : null,
                embed.getImage() != null ? embed.getImage().getUrl() : null,
                null,
                new WebhookEmbed.EmbedTitle(Optional.ofNullable(embed.getTitle()).orElse("Embed"), null),
                null,
                embedFields
        );
    }

    private List<WebhookClient> createWebhookClients(StandardGuildMessageChannel channel, int hookCount) {
        if (hookCount > 10) {
            throw new IllegalArgumentException("hookCount exceeds maximum hooks per guild of 10");
        }

        List<WebhookClient> webhookClients = new ArrayList<>(hookCount);
        for (int i = 0; i < hookCount; i++) {
            var transferHook = createWebhook(channel);
            if (transferHook == null) {
               break;
            }

            webhookClients.add(WebhookClientBuilder.fromJDA(transferHook)
                    .setThreadFactory((job) -> {
                        Thread thread = new Thread(job);
                        thread.setName("WebHook-Thread");
                        thread.setDaemon(true);
                        return thread;
                    })
                    .setWait(false)
                    .build()
            );
        }
        return webhookClients;
    }

    private Webhook createWebhook(StandardGuildMessageChannel channel) {
        Webhook transferHook = null;
        try {
            transferHook = channel.createWebhook("Transfer-Hook").complete();

        }
        catch (ErrorResponseException e) {
            Logger log = Logger.getLogger(Settings.LOGGER_NAME);
            if (e.getErrorCode() == ErrorResponse.MISSING_ACCESS.getCode() || e.getErrorCode() == ErrorResponse.MAX_WEBHOOKS.getCode()) {
                log.severe(e.getMeaning());
            }
            else if (e.getErrorCode() == ErrorResponse.MISSING_PERMISSIONS.getCode()) {
                log.severe("Missing permissions: " + e.getMessage());
            }
        }
        return transferHook;
    }

    private StandardGuildMessageChannel retrieveGuildMessageChannel(JDA jda, String channelID) {
        StandardGuildMessageChannel channel = null;
        try {
            channel = jda.getChannelById(TextChannel.class, channelID);
            if (channel == null) {
                channel = jda.getChannelById(NewsChannel.class, channelID);
            }
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return channel;
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        var slashCommand = Commands.slash(command.getName(), command.getDescription())
                .addOptions(OPTION_CHANNEL_FROM, OPTION_CHANNEL_TO, OPTION_START_FROM);

        api.upsertCommand(slashCommand).queue();
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        var slashCommand = Commands.slash(command.getName(), command.getDescription())
                .addOptions(OPTION_CHANNEL_FROM, OPTION_CHANNEL_TO, OPTION_START_FROM);

        guild.upsertCommand(slashCommand).queue();
    }
}
