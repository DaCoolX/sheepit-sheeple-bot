package bot.commands;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import net.dv8tion.jda.api.Permission;

import java.io.IOException;

public class CommandDeserializer extends JsonDeserializer<Command> {
    @Override
    public Command deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode commandNode = p.getCodec().readTree(p);
        String command;
        String output;
        Permission perm;
        String description = null;
        CommandCategories category;
        JsonNode tmpCommandValue;
        tmpCommandValue = commandNode.get("name");
        if(tmpCommandValue != null) {
            command = tmpCommandValue.textValue();
            if (command.equals("register")) {   //TODO remove this migration code
                return null;
            }
        }
        else {
            return null;
        }
        tmpCommandValue = commandNode.get("output");
        if(tmpCommandValue != null) {
            output = tmpCommandValue.textValue();
        }
        else {
            return null;
        }
        tmpCommandValue = commandNode.get("requiredPermission");
        if(tmpCommandValue != null) {
            String permString = tmpCommandValue.textValue();
            if (permString.equals("MESSAGE_READ") || permString.equals("MESSAGE_WRITE")) {
                permString = Permission.MESSAGE_SEND.name(); //TODO remove this migration code
            }
            perm = Permission.valueOf(permString);
        }
        else {
            return null;
        }
        tmpCommandValue = commandNode.get("description");
        if(tmpCommandValue != null) {
            description = tmpCommandValue.textValue();
        }

        tmpCommandValue = commandNode.get("category");
        if(tmpCommandValue != null) {
            category = CommandCategories.valueOf(tmpCommandValue.textValue());
        }
        else {
            category = CommandCategories.MISC;
        }

        return new Command(command, output, description, category, perm);
    }
}
