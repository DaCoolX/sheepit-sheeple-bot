package bot.commands;

import bot.commands.roleCommands.CleanHouseHandler;
import bot.errorhandlers.MessageDeleteErrorHandler;
import bot.settings.Settings;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.components.buttons.Button;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WipeHandler extends CleanHouseHandler {
    private static final String buttonID = "button_confirm_delete";
    private ExecutorService executor;
    private ButtonClickHandler buttonHandler;


    public WipeHandler() {
        this.executor = Executors.newFixedThreadPool(3);
        this.buttonHandler = new ButtonClickHandler();
    }

    @Override
    protected Optional<MessageEmbed> execute(Command command, SlashCommandInteractionEvent event) {
        try {
            TextChannel channel = event.getChannel().asTextChannel();
            if(!channel.getId().equals(Settings.getInstance().getRegistrationChannelID())) {
                event.reply("You are calling this command in a regular/public channel. Are you sure that you want to wipe this channel?")
                        .setEphemeral(true) //no one else should be able to press that button
                        .addActionRow(Button.danger(buttonID, "Confirm")).queue();
                return Optional.empty();
            }

            event.deferReply().queue();
            executor.submit(() -> wipeAllButFirst(channel, event));
        } catch(RejectedExecutionException | IllegalArgumentException | NullPointerException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, "Exception: ", e);
            return Optional.of(Settings.getInstance().createMessage(e.getMessage()));
        }
        return Optional.empty();
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandInteractionEvent event) {
        return event.getMember().hasPermission(command.getRequiredPermission()) ? RequirementsCheckResult.OK : RequirementsCheckResult.NO_PERMISSION;
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        if(!api.getRegisteredListeners().contains(buttonHandler))
            api.addEventListener(buttonHandler);
        super.registerCommand(command, api);
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        if(!guild.getJDA().getRegisteredListeners().contains(buttonHandler))
            guild.getJDA().addEventListener(buttonHandler);
        super.registerCommand(command, guild);
    }

    private class ButtonClickHandler extends ListenerAdapter {

        @Override
        public void onButtonInteraction(ButtonInteractionEvent event) {
            if(event.getComponentId().equals(buttonID)) {
                //disable button
                event.editButton(event.getButton().asDisabled()).queue();

                //wipe channel
                executor.submit(() -> wipeAllButFirst(event.getChannel().asTextChannel(), event));

                //delete ephemeral message
                event.getHook().editOriginal("Wipe confirmed").queue();
                event.getHook().deleteOriginal().queueAfter(Settings.getInstance().getDeleteDelaySeconds(), TimeUnit.SECONDS,
                        null, new MessageDeleteErrorHandler());
                Logger.getLogger(Settings.LOGGER_NAME).log(Level.INFO, () -> String.format("%s confirmed wipe in %s",
                        event.getMember().getUser().getAsTag(), event.getChannel()));
            }
        }
    }
}
