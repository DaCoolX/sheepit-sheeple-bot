package bot.settings;

import lombok.Getter;

@Getter
public enum DefaultSuperRoles {

    SHEEPMIN("307670315973214210"), SHEEPIT_OWNER("292834813210001408");

    private String id;

    DefaultSuperRoles(String id) {
        this.id = id;
    }
}
