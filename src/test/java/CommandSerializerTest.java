import bot.commands.Command;
import bot.commands.CommandCategories;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.dv8tion.jda.api.Permission;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;

class CommandSerializerTest {

    @Test
    void serialize() throws IOException {
        Command cmd = new Command("!pi-test", "This is a test", "test description", CommandCategories.MISC, Permission.MANAGE_CHANNEL);
        String json = new ObjectMapper()
                .writerWithDefaultPrettyPrinter().writeValueAsString(cmd);
        System.out.println(json);
        AtomicReference<JsonParser> parser = new AtomicReference<>();
        JsonNode node;
        assertDoesNotThrow(() -> parser.set(new ObjectMapper().createParser(json)));
        assertNotNull(parser.get());
        node = parser.get().getCodec().readTree(parser.get());
        assertEquals(node.get("name").textValue(), cmd.getName());
        assertEquals(node.get("output").textValue(), cmd.getOutput());
        assertEquals(node.get("requiredPermission").textValue(), cmd.getRequiredPermission().name());
        assertEquals(node.get("description").textValue(), cmd.getDescription());

        parser.get().close();
    }
}